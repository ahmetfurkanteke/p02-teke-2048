//
//  ViewController.swift
//  p02
//
//  Created by Ahmet Teke on 2/6/17.
//  Copyright © 2017 Ahmet Teke. All rights reserved.
//

import UIKit
import Darwin

class ViewController: UIViewController {

    // Score label
    @IBOutlet weak var score: UILabel!
    // Square collection
    @IBOutlet var squares: [UILabel]!
    // Colors
    let colors: [String: UIColor] = [
        "2": UIColor(red:0.75, green:0.67, blue:0.43, alpha:1.0),
        "4": UIColor(red:0.77, green:0.60, blue:0.39, alpha:1.0),
        "8": UIColor(red:0.77, green:0.60, blue:0.39, alpha:1.0),
        "16": UIColor(red:0.80, green:0.47, blue:0.31, alpha:1.0),
        "32": UIColor(red:0.82, green:0.40, blue:0.28, alpha:1.0),
        "64": UIColor(red:0.83, green:0.34, blue:0.24, alpha:1.0),
        "128": UIColor(red:0.85, green:0.27, blue:0.20, alpha:1.0),
        "256": UIColor(red:0.86, green:0.20, blue:0.17, alpha:1.0),
        "512": UIColor(red:0.88, green:0.14, blue:0.13, alpha:1.0),
        "1024": UIColor(red:0.89, green:0.07, blue:0.09, alpha:1.0),
        "2048": UIColor(red:0.91, green:0.01, blue:0.06, alpha:1.0),
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        startGame()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // initial start with 2 point 2 squares
    func startGame(){
        for _ in 1...2{
            let index = findEmptySquare()
            changeSquare(index: index, value: "2")
        }
    }
    // generic square value changer
    func changeSquare(index: Int, value: String){
        squares[index].text = value
        squares[index].backgroundColor = colors[value]
    }
    // tries to find an empty square, if can't find it alerts you lost
    func findEmptySquare() -> Int{
        var index: Int = 0
        var isThereAnyEmpty = false
        for square in squares{
            if square.text == ""{
                isThereAnyEmpty = true
            }
        }
        if isThereAnyEmpty{
            var stillLooking = true
            while stillLooking{
                index = Int(arc4random() % 16)
                if squares[index].text == ""{
                    stillLooking = false
                }
            }
        }else{
            let alertController = UIAlertController(title: "Sorry", message: "You lost this game!", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
        return index
    }
    
    
    //checks is there any 2048 if there is one you win
    func checkWon(){
        for square in squares{
            if square.text == "2048"{
                let alertController = UIAlertController(title: "Congratulations", message: "You won this game!", preferredStyle: UIAlertControllerStyle.alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                
            }
        }
    }
    
    // while everyone can move left it moves
    // adds random 2 point every button click
    // checks user won
    // other move functions works same
    @IBAction func leftMove(_ sender: Any) {
        while moveLeft(){
            print("Moved")
        }
        addRandom()
        checkWon()
    }
    
    
    @IBAction func rightMove(_ sender: Any) {
        while moveRight(){
            print("Moved")
        }
        addRandom()
        checkWon()

    }
    @IBAction func upMove(_ sender: Any) {
        while moveUp(){
            print("Moved")
        }
        addRandom()
        checkWon()
    }
    
    @IBAction func downMove(_ sender: Any) {
        while moveDown(){
            print("Moved")
        }
        addRandom()
        checkWon()

    }
    
    // indexes show which index need to move first
    // it moves until there is no empty square at left
    // if square at left has the same value with recent, they will appended on left side
    // other move functions works similar
    func moveLeft()->Bool{
        let indexes = [
            [1, 2, 3],
            [5, 6, 7],
            [9, 10, 11],
            [13 ,14 ,15]
        ]
        var canMove = false
        for index in indexes{
            for idx in index{
                if squares[idx].text != "" && squares[idx-1].text == ""{
                    squares[idx-1].text = squares[idx].text
                    squares[idx-1].backgroundColor = squares[idx].backgroundColor
                    
                    squares[idx].text = ""
                    squares[idx].backgroundColor =  UIColor(white: 1, alpha: 0)
                    
                    canMove = true
                }
                else if squares[idx].text != "" && squares[idx].text == squares[idx-1].text{
                    let value: Int = Int(squares[idx].text!)!
                    squares[idx-1].text = String(value * 2)
                    squares[idx-1].backgroundColor = colors[String(value * 2)]
                    
                    squares[idx].text = ""
                    squares[idx].backgroundColor =  UIColor(white: 1, alpha: 0)
                    scoreUpdate(value: value * 2)

                    canMove = true

                }
            }
        }
        return canMove
    }
    func moveRight()->Bool{
        let indexes = [
            [2, 1, 0],
            [6, 5, 4],
            [10, 9, 8],
            [14, 13, 12]
        ]
        var canMove = false
        for index in indexes{
            for idx in index{
                if squares[idx].text != "" && squares[idx+1].text == ""{
                    squares[idx+1].text = squares[idx].text
                    squares[idx+1].backgroundColor = squares[idx].backgroundColor
                    
                    squares[idx].text = ""
                    squares[idx].backgroundColor =  UIColor(white: 1, alpha: 0)
                    
                    canMove = true
                }
                else if squares[idx].text != "" && squares[idx].text == squares[idx+1].text{
                    let value: Int = Int(squares[idx].text!)!
                    squares[idx+1].text = String(value * 2)
                    squares[idx+1].backgroundColor = colors[String(value * 2)]
                    
                    squares[idx].text = ""
                    squares[idx].backgroundColor =  UIColor(white: 1, alpha: 0)
                    scoreUpdate(value: value * 2)

                    canMove = true
                    
                }
            }
        }
        return canMove
    }
    
    func moveUp()->Bool{
        let indexes = [
            [4, 8, 12],
            [5, 9, 13],
            [6, 10, 14],
            [7, 11, 15]
        ]
        var canMove = false
        for index in indexes{
            for idx in index{
                if squares[idx].text != "" && squares[idx-4].text == ""{
                    squares[idx-4].text = squares[idx].text
                    squares[idx-4].backgroundColor = squares[idx].backgroundColor
                    
                    squares[idx].text = ""
                    squares[idx].backgroundColor =  UIColor(white: 1, alpha: 0)
                    
                    canMove = true
                }
                else if squares[idx].text != "" && squares[idx].text == squares[idx-4].text{
                    let value: Int = Int(squares[idx].text!)!
                    squares[idx-4].text = String(value * 2)
                    squares[idx-4].backgroundColor = colors[String(value * 2)]
                    
                    squares[idx].text = ""
                    squares[idx].backgroundColor =  UIColor(white: 1, alpha: 0)
                    scoreUpdate(value: value * 2)

                    canMove = true
                    
                }
            }
        }
        return canMove
    }
    
    func moveDown()->Bool{
        let indexes = [
            [8, 4, 0],
            [9, 5, 1],
            [10, 6, 2],
            [11, 7, 3]
        ]
        var canMove = false
        for index in indexes{
            for idx in index{
                if squares[idx].text != "" && squares[idx+4].text == ""{
                    squares[idx+4].text = squares[idx].text
                    squares[idx+4].backgroundColor = squares[idx].backgroundColor
                    
                    squares[idx].text = ""
                    squares[idx].backgroundColor =  UIColor(white: 1, alpha: 0)
                    
                    canMove = true
                }
                else if squares[idx].text != "" && squares[idx].text == squares[idx+4].text{
                    let value: Int = Int(squares[idx].text!)!
                    squares[idx+4].text = String(value * 2)
                    squares[idx+4].backgroundColor = colors[String(value * 2)]
                    squares[idx].text = ""
                    squares[idx].backgroundColor =  UIColor(white: 1, alpha: 0)
                    scoreUpdate(value: value * 2)
                    canMove = true
                    
                }
            }
        }
        return canMove
    }
    // adds a random indexed square a value
    func addRandom(){
        let index = findEmptySquare()
        changeSquare(index: index, value: "2")
    }
    
    // updates score
    func scoreUpdate(value: Int){
        let new_score =  Int(score.text!)! + value
        score.text = String(new_score)
    }

}
    











